import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    reservaItems: [],

  },
  mutations: {
    setReservaItemsMutation(state, datos) {
      state.reservaItems.push(datos)
    },
    removeReservaItemsMutation(state, index) {
      state.reservaItems.splice(index, 1)
    },
  },
  actions: {
    addReservaItemsAction(context, datos) {
      context.commit('setReservaItemsMutation', datos)
    },
    removeReservaItemsAction(context, index) {
      context.commit('removeReservaItemsMutation', index)
    },
  },
  getters: {
    getReservaItems(state) {
      return state.reservaItems
    },
  },
  modules: {
  }
})
